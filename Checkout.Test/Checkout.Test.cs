﻿using Checkout.Model;
using NUnit.Framework;
using System;
using System.Collections.Generic;

namespace Checkout.Tests
{
    public class CheckoutTest
    {
        private Checkout _checkout;
        private SpecialOffers _specialOffers;
        private IDictionary<SpecialOffer, int> _specialOffer;

        private readonly Item _itemA = new Item
        {
            StockKeepingUnit = "A",
            Price = 50
        };
        private readonly Item _itemB = new Item
        {
            StockKeepingUnit = "B",
            Price = 30
        };
        private readonly Item _itemC = new Item
        {
            StockKeepingUnit = "C",
            Price = 20
        };
        private readonly Item _itemD = new Item
        {
            StockKeepingUnit = "D",
            Price = 15
        };
        private readonly SpecialOffer _specialOfferA = new SpecialOffer
        {
            StockKeepingUnit = "A",
            Amount = 3,
            Price = 130
        };
        private readonly SpecialOffer _specialOfferB = new SpecialOffer
        {
            StockKeepingUnit = "B",
            Amount = 2,
            Price = 45
        };


        [SetUp]
        public void SetUp()
        {
            _checkout = new Checkout();
            _specialOffers = new SpecialOffers();

            if (_specialOffers == null) return;
            _specialOffers.AddSpecialOffer(_specialOfferA);
            _specialOffers.AddSpecialOffer(_specialOfferB);
        }

        [Test]
        public void GivenIHaveOneItem_IScanThem_ShouldReturn220GBP()
        {
            if (_checkout == null) return;
            _checkout.Scan(_itemA);

            var totalPrice = _checkout.GetTotalPrice(new Dictionary<SpecialOffer, int>());
            Assert.AreEqual(50, totalPrice);
        }

        [Test]
        public void GivenIHaveProvidedNull_ICheckout_ShouldReturn0()
        {
            _checkout.Scan(null);
            Assert.AreEqual(0, _checkout.Items.Count);
        }

        [Test]
        public void GivenIHaveTwoItems_IScanThem_ShouldHaveTwoItemsInBasket()
        {
            _checkout.Scan(_itemA);
            _checkout.Scan(_itemA);
            var offersApplied = _specialOffers.ApplySpecialOffers(_checkout.Items);

            Assert.AreEqual(2, _checkout.Items.Count);
        }

        [Test]
        public void GivenIHaveOneItemA_ICheckout_ShouldBeTotalOf50()
        {
            _checkout.Scan(_itemA);
            var offersApplied = _specialOffers.ApplySpecialOffers(_checkout.Items);

            var totalPrice = _checkout.GetTotalPrice(offersApplied);

            Assert.AreEqual(50, totalPrice);
        }

        [Test]
        public void GivenIHaveOneItemsAandOneItemB_ICheckout_ShouldBeTotalOf80()
        {
            _checkout.Scan(_itemA);
            _checkout.Scan(_itemB);
            var offersApplied = _specialOffers.ApplySpecialOffers(_checkout.Items);

            var totalPrice = _checkout.GetTotalPrice(offersApplied);

            Assert.AreEqual(80, totalPrice);
        }

        [Test]
        public void GivenIHaveTwoItemsD_ICheckout_ShouldBeTotalOf30()
        {
            _checkout.Scan(_itemD);
            _checkout.Scan(_itemD);
            var offersApplied = _specialOffers.ApplySpecialOffers(_checkout.Items);

            var totalPrice = _checkout.GetTotalPrice(offersApplied);

            Assert.AreEqual(30, totalPrice);
        }

        [Test]
        public void GivenIHaveTwoItemAandOneItemB_ICheckout_ShouldBeTotalOf130()
        {
            _checkout.Scan(_itemA);
            _checkout.Scan(_itemA);
            _checkout.Scan(_itemB);
            var offersApplied = _specialOffers.ApplySpecialOffers(_checkout.Items);

            var totalPrice = _checkout.GetTotalPrice(offersApplied);

            Assert.AreEqual(130, totalPrice);
        }

        [Test]
        public void GivenIHaveOneOfEachItem_ICheckout_ShouldBeTotalOf115()
        {
            _checkout.Scan(_itemA);
            _checkout.Scan(_itemB);
            _checkout.Scan(_itemC);
            _checkout.Scan(_itemD);
            var offersApplied = _specialOffers.ApplySpecialOffers(_checkout.Items);

            var totalPrice = _checkout.GetTotalPrice(offersApplied);

            Assert.AreEqual(115, totalPrice);
        }

        [Test]
        public void GivenIHaveThreeItemsA_ICheckout_ShouldBeTotalOf130()
        {
            _checkout.Scan(_itemA);
            _checkout.Scan(_itemA);
            _checkout.Scan(_itemA);
            var offersApplied = _specialOffers.ApplySpecialOffers(_checkout.Items);

            var totalPrice = _checkout.GetTotalPrice(offersApplied);

            Assert.AreEqual(130, totalPrice);
        }

        [Test]
        public void GivenIHaveFourItemsA_ICheckout_ShouldBeTotalOf180()
        {
            _checkout.Scan(_itemA);
            _checkout.Scan(_itemA);
            _checkout.Scan(_itemA);
            _checkout.Scan(_itemA);
            var offersApplied = _specialOffers.ApplySpecialOffers(_checkout.Items);

            var totalPrice = _checkout.GetTotalPrice(offersApplied);

            Assert.AreEqual(180, totalPrice);
        }

        [Test]
        public void GivenIHaveFourItemsAAndOneB_ICheckout_ShouldBeTotalOf210()
        {
            _checkout.Scan(_itemA);
            _checkout.Scan(_itemA);
            _checkout.Scan(_itemA);
            _checkout.Scan(_itemA);
            _checkout.Scan(_itemB);
            var offersApplied = _specialOffers.ApplySpecialOffers(_checkout.Items);

            var totalPrice = _checkout.GetTotalPrice(offersApplied);

            Assert.AreEqual(210, totalPrice);
        }

        [Test]
        public void GivenIHaveFourItemsAAndTwoB_ICheckout_ShouldBeTotalOf225()
        {
            _checkout.Scan(_itemA);
            _checkout.Scan(_itemA);
            _checkout.Scan(_itemA);
            _checkout.Scan(_itemA);
            _checkout.Scan(_itemB);
            _checkout.Scan(_itemB);
            var offersApplied = _specialOffers.ApplySpecialOffers(_checkout.Items);

            var totalPrice = _checkout.GetTotalPrice(offersApplied);

            Assert.AreEqual(225, totalPrice);
        }

        [Test]
        public void GivenIHaveSixItemsA_ICheckout_ShouldBeTotalOf130()
        {
            _checkout.Scan(_itemA);
            _checkout.Scan(_itemA);
            _checkout.Scan(_itemA);
            _checkout.Scan(_itemA);
            _checkout.Scan(_itemA);
            _checkout.Scan(_itemA);
            var offersApplied = _specialOffers.ApplySpecialOffers(_checkout.Items);

            var totalPrice = _checkout.GetTotalPrice(offersApplied);

            Assert.AreEqual(260, totalPrice);
        }
    }
}
