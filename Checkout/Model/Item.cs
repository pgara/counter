﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Checkout.Model
{
    public class Item
    {
        public string StockKeepingUnit { get; set; }
        public double Price { get; set; }
    }
}
