﻿using Checkout.Model;
using System;
using System.Collections.Generic;
using System.Text;

namespace Checkout.Abstract
{
    public interface ICheckout
    {
        void Scan(Item item);
        double GetTotalPrice(IDictionary<SpecialOffer, int> specialOffers);
    }
}
