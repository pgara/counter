﻿using Checkout.Abstract;
using Checkout.Model;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Checkout
{
    public class Checkout : ICheckout
    {
        public IList<Item> Items { get; private set; }

        public Checkout()
        {
            Items = new List<Item>();
        }
        public double GetTotalPrice(IDictionary<SpecialOffer, int> specialOffers)
        {
            double totalPrice = 0.0;
            foreach (var specialOffer in specialOffers)
            {
                totalPrice += specialOffer.Key.Price * specialOffer.Value;

                for (int i = 0; i < specialOffer.Key.Amount * specialOffer.Value; i++)
                {
                    Items.Remove(Items.FirstOrDefault(x => x.StockKeepingUnit == specialOffer.Key.StockKeepingUnit));
                }
            }
            totalPrice += Items.Sum(x => x.Price);

            return totalPrice;
        }

        public void Scan(Item item)
        {
            if (item == null) return;
            Items.Add(item);
        }
    }
}
