﻿using Checkout.Model;
using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;

namespace Checkout
{
    public class SpecialOffers : List<SpecialOffer>
    {
        public void AddSpecialOffer(SpecialOffer specialOffer)
        {
            if (specialOffer == null) return;
            Add(specialOffer);
        }
        public IDictionary<SpecialOffer, int> ApplySpecialOffers(IEnumerable<Item> items)
        {
            IDictionary<SpecialOffer, int> offersApplied = new Dictionary<SpecialOffer, int>();

            var itemsSKUs = items.Select(x => x.StockKeepingUnit);
            var specialOffersSKUs = this.Select(x => x.StockKeepingUnit);
            var commonSKUs = specialOffersSKUs.Intersect(itemsSKUs);

            //Check if there are any special offers that can be applied
            //If not, do nothing and return
            if (commonSKUs.Count() == 0) return offersApplied;

            foreach (var sku in commonSKUs)
            {
                int specialOfferAmount = this.FirstOrDefault(x => x.StockKeepingUnit == sku).Amount;
                var basketItemsWithMatchingSKU = items.Where(x => x.StockKeepingUnit == sku);

                if (specialOfferAmount > 0 && basketItemsWithMatchingSKU.Count() >= specialOfferAmount)
                {
                    int noOfTimesApplied = basketItemsWithMatchingSKU.Count() / specialOfferAmount;
                    offersApplied.Add(this.FirstOrDefault(x => x.StockKeepingUnit == sku), noOfTimesApplied);
                }
            }
            return offersApplied;
        }
    }
}
